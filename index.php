<?php 
include("header.php");
?>

  <!-- Main jumbotron for a primary marketing message or call to action -->

<h1 class="step-title"></h1><br>
  <div class="container">
    <section class="palette-container">
      <div class="palette-item">
        <span class="color-item">
          <img class="color-icon" />
        </span>
      </div>
    </section>

    <section class="design-area">
      Design Area
      <img src="src/images/rugback.png" id="design-image" />
    </section>

    <section class="layers-container">
      <div class="rug-part-item">
        <span class="layer-item">
          <img class="layer-image" img="" /> 
        </span>
      </div>
    </section>

    <section class="step-container">
      <div class="step-body">
        <h1>Welcome!</h1>
        In order to create a new Rug give a name to Your rug an click the button below!
        <div class="buttons-board">
          <input type="text" id="rug-name-field"></input>
          <button id="create-rug-btn" class="create-rug-btn">Start</button>
        </div>
      </div>
    </section>

    <div class="button-board">
      <button id="next-step-btn" class="next-step-btn">Next</button>
      <button id="finish-step-btn" class="finish-step-btn">Finish</button>
    </div>

  </div><!-- // .container -->

  <hr>
  <input type="text" id="rug-id-field"></input>
  <button id="show-rug-btn">Show Rug</button>

<?php
include("footer.php");
?>