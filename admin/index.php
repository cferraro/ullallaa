<?php 
require __DIR__ . '/vendor/autoload.php';

use DI\Container;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Ullallaa\BaseClasses\PartType as PartTypeObject;
use Ullallaa\Database\DbQuery;
use Ullallaa\Database\DbCore;
use Ullallaa\Database\DbPDOCore;
use Ullallaa\Database\DbMySQLiCore;


abstract class Db extends DbCore {};
class DbPDO extends DbPDOCore {};
class DbMySQLi extends DbMySQLiCore {};

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** CORS Middleware */
$config = array(
	/** MySQL database name */
	'database_name' => 'ullallaa',

  	/** MySQL Prod */
	'database_prod_host' => 'mysql685.loopia.se',
	/** MySQL database username Prod */
	'database_prod_user' => 'claudio1@u272959',
	/** MySQL database password Prod */ 
	'database_prod_password' => 'let_the_fun_begin',
  /** MySQL database name Prod */ 
	'database_prod_name' => 'ullallaa_com',

	/** MySQL hostname */
	'database_host' => 'localhost',
	/** MySQL database username */
	'database_user' => 'root',
	/** MySQL database password */ 
	'database_password' => '',
	/** MySQL Database Table prefix. */
	'database_prefix' => '',
	/** preferred database */
	'database_engine' => 'DbPDO',
	/** API CORS */
	'cors' => [
		'enabled' => true,
		'origin' => '*', // can be a comma separated value or array of hosts
		'headers' => [
			'Access-Control-Allow-Headers' => 'Origin, X-Requested-With, Authorization, Cache-Control, Content-Type, Access-Control-Allow-Origin',
			'Access-Control-Allow-Credentials' => 'true',
			'Access-Control-Allow-Methods' => 'GET,PUT,POST,DELETE,OPTIONS,PATCH'
		]
	]
);


define('_DB_PROD_', false);

if (!_DB_PROD_) {
  define('_BASE_PATH_', '');
	define('_DB_SERVER_', $config['database_host']);
	define('_DB_USER_', $config['database_user']);
	define('_DB_PASSWD_', $config['database_password']);
  define('_DB_NAME_', $config['database_name']);
} else {
  define('_BASE_PATH_', '');
	define('_DB_SERVER_', $config['database_prod_host']);
	define('_DB_USER_', $config['database_prod_user']);
	define('_DB_PASSWD_', $config['database_prod_password']);
  define('_DB_NAME_', $config['database_prod_name']);
}

define('_DB_PREFIX_',  $config['database_prefix']);
define('_MYSQL_ENGINE_',  $config['database_engine']);


// Create Container
$container = new Container();
AppFactory::setContainer($container);

// Set view in Container
$container->set('view', function($container) {
    return Twig::create('partials');
});


$container->set('upload_directory', __DIR__ . '../uploads');

// Create App
$app = AppFactory::create();
$app->setBasePath('/ullalla/admin');

// Add Twig-View Middleware
$app->add(TwigMiddleware::createFromContainer($app));

// Routes
$app->get('/', function ($request, $response, $args) {
  $partTypeApi = new PartTypeObject();  
  return $this->get('view')->render($response, 'home.twig', [
    'parttypes' => $partTypeApi->getPartTypes()
  ]);
})->setName('dashboard');

$app->get('/{name}', function ($request, $response, $args) {
  $partTypeApi = new PartTypeObject();
  return $this->get('view')->render($response, $args['name'] . '.twig', [
    'name' => $args['name'],
    'parttypes' => $partTypeApi->getPartTypes()
  ]);
})->setName('adminpage');

/* LAYERS */
$app->get('/layer/{idlayertype}', function ($request, $response, $args) {

  $partTypeApi = new PartTypeObject();
  $idlayerType = intval($args['idlayertype']);
  $selectedPartTypeName = '';
  if (is_int($idlayerType)) { 
	$selectedPartType = $partTypeApi->getPartType($idlayerType)[0];
	$partTypeName = $selectedPartType['name'];
	$partTypeWithParent = $selectedPartType['withparent'];
	$partTypeParent = $selectedPartType['parent'];
	$selectedPartTypeName = $partTypeName;
  }
  return $this->get('view')->render($response, 'layers.twig', [
    'parttypeId' => $idlayerType,
	'parttypeName' => $selectedPartTypeName,
    'parttypes' => $partTypeApi->getPartTypes(),
	'parent' => $partTypeParent,
	'withparent' => $partTypeWithParent
  ]);
})->setName('layerpage');

// Run app
$app->run();


?>
