<?php declare(strict_types=1);

namespace Ullallaa\BaseClasses;

use Db;
use Ullallaa\Database\DbQuery;


class PartType {

    public function getPartTypes() {
        // Build query
        $sql = new DbQuery();
        // Build SELECT
        $sql->select('part_types.*');
        // Build FROM
        $sql->from('part_types', 'part_types');
        $sql->orderBy('part_types.orderindex');
        
        $parttypes = Db::getInstance()->executeS($sql);
        return $parttypes;
    }


    public function getPartType( $partId ) {

        $sql = new DbQuery();
        // Build SELECT
        $sql->select('part_types.*');
        // Build FROM
        $sql->from('part_types', 'part_types');
        $sql->where('part_types.id = ' . $partId);
        $parttypes = Db::getInstance()->executeS($sql);

        return $parttypes;
	}

}

