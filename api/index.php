<?php 
/**
 * @copyright  2020 Ullallaa
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

 // Namespaces
define('API_NAMESPACE',          'Ullallaa');
define('API_DIR_ROOT',            dirname(__FILE__));
define('API_DIR_CLASSES',         API_DIR_ROOT . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR);
define('API_DIR_CONTROLLERS',     API_DIR_ROOT . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR);

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
require_once API_DIR_ROOT . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php'; 
require_once API_DIR_ROOT . DIRECTORY_SEPARATOR . 'autoload.php'; 
require_once API_DIR_ROOT . DIRECTORY_SEPARATOR . 'functions.php'; 

use Ullallaa\Api;
use Ullallaa\Database\DbQuery;
use Ullallaa\Database\DbCore;
use Ullallaa\Database\DbPDOCore;
use Ullallaa\Database\DbMySQLiCore;

abstract class Db extends DbCore {};
class DbPDO extends DbPDOCore {};
class DbMySQLi extends DbMySQLiCore {};


/** CORS Middleware */
$config = array(

	/** MySQL Prod */
	'database_prod_host' => 'mysql685.loopia.se',
	/** MySQL database username Prod */
	'database_prod_user' => 'claudio1@u272959',
	/** MySQL database password Prod */ 
	'database_prod_password' => 'let_the_fun_begin',
  	/** MySQL database name Prod */ 
	'database_prod_name' => 'ullallaa_com',


	/** MySQL database name */
	'database_name' => 'ullallaa',
	/** MySQL hostname */
	'database_host' => 'localhost',
	/** MySQL database username */
	'database_user' => 'root',
	/** MySQL database password */ 
	'database_password' => '',
	/** MySQL Database Table prefix. */
	'database_prefix' => '',
	/** preferred database */
	'database_engine' => 'DbPDO',
	/** API CORS */
	'cors' => [
		'enabled' => true,
		'origin' => '*', // can be a comma separated value or array of hosts
		'headers' => [
			'Access-Control-Allow-Headers' => 'Origin, X-Requested-With, Authorization, Cache-Control, Content-Type, Access-Control-Allow-Origin',
			'Access-Control-Allow-Credentials' => 'true',
			'Access-Control-Allow-Methods' => 'GET,PUT,POST,DELETE,OPTIONS,PATCH'
		]
	]
);

define('_DB_PROD_', false);

if (!_DB_PROD_) {
	define('_DB_SERVER_', $config['database_host']);
	define('_DB_USER_', $config['database_user']);
	define('_DB_PASSWD_', $config['database_password']);
  	define('_DB_NAME_', $config['database_name']);
} else {
	define('_DB_SERVER_', $config['database_prod_host']);
	define('_DB_USER_', $config['database_prod_user']);
	define('_DB_PASSWD_', $config['database_prod_password']);
  	define('_DB_NAME_', $config['database_prod_name']);
}

define('_DB_PREFIX_',  $config['database_prefix']);
define('_MYSQL_ENGINE_',  $config['database_engine']);


try {

/** API Construct */
$api = new Api([
	'mode' => 'development',
    'debug' => true
]);



$api->add(new \Ullallaa\Slim\CorsMiddleware());
$api->config('debug', true);

/**
 * Request Payload
 */
$params = $api->request->get();
$requestPayload = $api->request->post();


$api->group('/v1', function () use ($api) {

	/* Colors */
	$api->get('/colors?', '\Ullallaa\v1\Color:getColors')->name('get_colors');
	
	/** Get filtered Colors */
	$api->get('/colors/:colorId?', '\Ullallaa\v1\Color:getColor')->name('get_color');

	$api->get('/colors/part_types/:partId?', '\Ullallaa\v1\Color:getColorByPartId')->name('get_colors_by_partid');

	/** Colors Admin **/
	/** Add a Color */
	$api->post('/colors?', '\Ullallaa\v1\Color:addColor')->name('add_color');

	/** Update a single Color */
	$api->patch('/colors/:colorId?', '\Ullallaa\v1\Color:updateColor')->name('update_color');

	$api->delete('/colors/:colorId?', '\Ullallaa\v1\Color:deleteColor')->name('delete_color');
	/* Color Admins */


	$api->get('/parttypes?', '\Ullallaa\v1\PartType:getPartTypes')->name('get_part_types');

	$api->get('/frontsteps?', '\Ullallaa\v1\FrontStep:getFrontSteps')->name('get_front_steps');


	$api->get('/frontsteps/:stepId(/:parentRugPart)', '\Ullallaa\v1\FrontStep:getFrontStepDataByStepId')->name('get_front_steps');

	/** Get all Layers */
	$api->get('/layers?', '\Ullallaa\v1\Layer:getLayers')->name('get_layers');
	
	/** Get layers by part type id */
	$api->get('/layers/:partTypeId?', '\Ullallaa\v1\Layer:getLayersByPartTypeId')->name('get_layers_by_part_type');
	
	/** Get layers by rug part id */
	$api->get('/layers/rugpart/:rugPartId?', 'Ullallaa\v1\Layer:getLayersByRugPartId')->name('get_layers_by_rug_part_id');

	/** Add a Layer */
	$api->post('/layers?', '\Ullallaa\v1\Layer:addLayer')->name('add_layers');

	/** Delete a Layer */
	$api->delete('/layers/:layerId?', '\Ullallaa\v1\Layer:deleteLayer')->name('delete_layer');

	$api->get('/rugparts/:typeId?', '\Ullallaa\v1\RugPart:getRugPartByTypeId')->name('get_rug_parts');

	/* Rug */
	$api->get('/rug/:rugid', '\Ullallaa\v1\Rug:getRug')->name('get_rug');
	
	/** Get layers by part type id */
	$api->patch('/rug/:rugid', '\Ullallaa\v1\Rug:patchRug')->name('patch_rug');
	
	/** Get layers by rug part id */
	$api->post('/rug?', 'Ullallaa\v1\Rug:addRug')->name('add_rug');

	/** Add a Layer */
	$api->post('/layers?', '\Ullallaa\v1\Layer:addLayer')->name('add_layers');



});


$api->notFound(function () use ($api) {
	$api->response([
		'success' => false,
		'error' => 'Resource Not Found'
	]);
	return $api->stop();
});

$api->response()->header('Content-Type', 'application/json; charset=utf-8');
$api->run();
}
catch (Throwable $t)
{
    echo 'Caught exception: ',  $t->getMessage(), "\n";
}