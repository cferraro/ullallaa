<?php
/**
 * @copyright  2020 Ullallaa
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

namespace Ullallaa;

use Ullallaa\Api;

abstract class Route
{
    /**
     * @var \Slim\Slim
     */
    protected $api;

    public function __construct(Api $api)
    {
        $this->api = $api;
    }
}
