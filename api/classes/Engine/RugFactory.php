<?php declare(strict_types=1);

namespace Ullallaa\Engine;

//use Ullallaa\Rugs\Parts\Rug;
use Ullallaa\Engine\Builders\RugBuilder;
use Ullallaa\Model\Layer as LayerObject;
use Ullallaa\Model\Color as ColorObject;
/**
 * 
 */
class RugFactory
{
    private $builder;

    function __construct() {
        $this->builder = new RugBuilder();
    }

    public function initRug($name)
    {
        return $this->builder->initRug($name);
    }

    public function loadComponents($json) {

        if ($json != null && $json != '') {

            $array = json_decode( $json, true );
            foreach($array as $item) {
                $id = $item['type_id'];
                $this->builder->addPart($id);

                $color = $item['color'];
                $this->builder->addPartColor($id, $color);

                $layers = $item['layers'];
                $this->builder->addPartLayer($id, $layers);

                $parent = $item['parent'];
                $this->builder->addPartParent($id, $parent);

                $child = $item['child'];
                $this->builder->addPartChild($id, $child);
            }

        }
    }

    public function addComponent($part_type, $color, $layer, $parent, $child) {
        $this->builder->addPart($part_type);
        $this->builder->addPartColor($part_type, $color);
        $this->builder->addPartLayer($part_type, $layer);
        $this->builder->addPartParent($part_type, $parent);
        $this->builder->addPartChild($part_type, $child);
    }

    public function getComponents() {
        return $this->builder->getParts();
    }

    public function getRug() {
        $x = 500; $y = 500;
        $final_img = imagecreatetruecolor($x, $y); // where x and y are the dimensions of the final image

        $parts = $this->getComponents();
        $output_to_browser = true;


        $images = Array();
        foreach ($parts as &$part) {

            $layers = $part['layers'];

            foreach ($layers as &$layer) {

                $layerObj = new LayerObject($layer);
                $filename = __DIR__ . '/../../../' . $layerObj->filename;
                $images[] = imagecreatefrompng($filename);
                $currentImage = $images[count($images) - 1];
                list($width, $height) = getimagesize($filename);

                if (!$part['parent']) {
                    $colorId = $part['color'];
                    if ($colorId) {
                        $color = new ColorObject($colorId);
                        // color item
                        list($r, $g, $b) = sscanf($color->colorcode, "#%02x%02x%02x");
                        //$brightness = 0.2126*$r + 0.7152*$g + 0.0722*$b;
                        $brightness = 0.299*$r + 0.587*$g + 0.114*$b;
                        imagefilter($currentImage, IMG_FILTER_BRIGHTNESS, -intval($brightness));
                        imagefilter($currentImage, IMG_FILTER_COLORIZE, $r, $g, $b);
                    }
                }
                imagealphablending($final_img, true);
                imagesavealpha($final_img, true);
                imagecopyresized($final_img, $currentImage, 0, 0, 0, 0, $x, $y, $width, $height);
            }
        }


        $outjson = json_encode($parts);
        if( $output_to_browser ) {

            ob_start();
            imagepng($final_img);
            $imagedata = ob_get_clean();
            return ['data' => base64_encode($imagedata), 'json' => $outjson];

        } else {
            imagepng($final_img, 'C:\wamp64\www\ullalla\final_img.png');
        }
    }
}