<?php declare(strict_types=1);

namespace Ullallaa\Engine\Builders;

use Ullallaa\Rugs\Parts\Border;
use Ullallaa\Rugs\Parts\Pattern;
use Ullallaa\Rugs\Parts\Rope;
use Ullallaa\Rugs\Parts\Rug;
use Ullallaa\Rugs\Parts\Warp;
use Ullallaa\Rugs\Parts\Yarn;

use Ullallaa\Engine\Builders\Builder;
use Ullallaa\Model\Rug as RugObject;

class RugBuilder implements Builder
{
    private $rugDB;
    private $rug = Array();

    public function initRug($name)
    {
        $this->rugDB = new RugObject();
		$this->rugDB->name = $name;
		$ok = $this->rugDB->save();
         
        return $ok ? $this->rugDB : null;
    }

    public function addPart($id)
    {
        $this->rug[$id]['type_id'] = $id;
    }

    public function addPartColor($id, $color)
    {
        $this->rug[$id]['color'] = $color ;
    }

    public function addPartLayer($id, $layers)
    {
        $this->rug[$id]['layers'] = $layers;
    }
    
    public function addPartParent($id, $parent)
    {
        $this->rug[$id]['parent'] = $parent;
    }
    
    public function addPartChild($id, $child)
    {
        $this->rug[$id]['child'] = $child;
    }

    public function getParts() 
    {
        return $this->rug;
    }

    public function getRug($id): Rug
    {
        return $this->rug[$id];
    }
}