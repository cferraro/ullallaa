<?php declare(strict_types=1);

namespace Ullallaa\Engine\Builders;

use Ullallaa\Rugs\Parts\Rug;

interface Builder
{
    public function initRug($name);

    public function addPart($id);

    public function addPartColor($id, $color);

    public function addPartLayer($id, $layers);

    public function addPartParent($id, $parent);

    public function addPartChild($id, $child);

    public function getParts();

    public function getRug($id): Rug;
}