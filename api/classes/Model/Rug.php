<?php declare(strict_types=1);

namespace Ullallaa\Model;

use Db;
use Ullallaa\Database\DbQuery;
use Ullallaa\ObjectModel;

class Rug extends ObjectModel {
	/** @var $id Product ID */
	public $id;
	
    /** @var string $name */
	public $name;

	/** @var int $rug_parts */
    public $rug_parts;


	/**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'rug',
        'primary' => 'id',
        'fields' => array(
			'name' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isString', 'size' => 255),
            'rug_parts' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isString', 'size' => 10000)
        )
    );

     /**
     * constructor.
     *
     * @param null $id
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
	}
}