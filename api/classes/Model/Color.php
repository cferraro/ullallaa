<?php declare(strict_types=1);

namespace Ullallaa\Model;

use Db;
use Ullallaa\Database\DbQuery;
use Ullallaa\ObjectModel;

class Color extends ObjectModel {
	/** @var $id Product ID */
	public $id;
	
	/** @var string $colorcode */
    public $colorcode;

    /** @var string $name */
	public $name;

	/** @var int $type_id */
    public $part_type_id;

    public function getByPartType($part_type_id) {

		// Build query
		$sql = new DbQuery();
		// Build SELECT
		$sql->select('colors.*');
		// Build FROM
		$sql->from('colors', 'colors');

		$where_clause = '';
		if($part_type_id) {
			$where_clause = 'part_type_id = ' . pSQL($part_type_id);
		}
		$sql->where($where_clause);
		$colors = Db::getInstance()->executeS($sql);

		return $colors;
    }
	/**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'colors',
        'primary' => 'id',
        'fields' => array(
			'name' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isString', 'size' => 255),
            'colorcode' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isString', 'size' => 12),
            'part_type_id' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'size' => 11)
        )
    );

     /**
     * constructor.
     *
     * @param null $id
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
	}
}