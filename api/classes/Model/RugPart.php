<?php declare(strict_types=1);

namespace Ullallaa\Model;

use Db;
use Ullallaa\Database\DbQuery;
use Ullallaa\ObjectModel;

class RugPart extends ObjectModel {
	/** @var $id Product ID */
	public $id;
	
    /** @var string $name */
	public $name;

	/** @var int $rug_part_id */
    public $type_id;

    /** @var int $parent */
    public $parent;


	public function addRugPart($name, $typeid, $parent_id) {
		$this->name = $name;
		$this->type_id = $typeid;
        $this->parent = $parent_id;

		$ok = $this->save();
		if (!$ok) {
			return null;
		}
        return $this->id;
	}

	/**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'rug_parts',
        'primary' => 'id',
        'fields' => array(
			'name' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isString', 'size' => 255),
            'type_id' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'size' => 11),
            'parent' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'size' => 11)
        )
    );

     /**
     * constructor.
     *
     * @param null $id
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
	}
}