<?php declare(strict_types=1);

namespace Ullallaa\Model;

use Db;
use Ullallaa\Database\DbQuery;
use Ullallaa\ObjectModel;

class PartType extends ObjectModel {
	/** @var $id Product ID */
	public $id;

    /** @var string $name */
	public $name;

    /** @var string $description */
    public $description;

    /** @var int $orderindex */
    public $orderindex;

    /** @var bool $withparent */
    public $withparent;


    public function getPartTypeByStep($stepId) {

		// Build query
		$sql = new DbQuery();
		// Build SELECT
		$sql->select('part_types.*');
		// Build FROM
		$sql->from('part_types', 'part_types');

		$where_clause = '';
		if($stepId) {
			$where_clause = 'id = ' . pSQL($stepId);
		}
		$sql->where($where_clause);
		$part_type = Db::getInstance()->executeS($sql);

		return $part_type;
    }
	/**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'part_types',
        'primary' => 'id',
        'fields' => array(
			'name' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isString', 'size' => 255),
            'description' => array('type' => self::TYPE_STRING, 'required' => false, 'validate' => 'isString', 'size' => 255),
            'orderindex' => array('type' => self::TYPE_INT, 'required' => false, 'validate' => 'isInt', 'size' => 11),
            'withparent' => array('type' => self::TYPE_BOOL, 'required' => false, 'validate' => 'isBool', 'size' => 11)
        )
    );

     /**
     * constructor.
     *
     * @param null $id
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
	}
}