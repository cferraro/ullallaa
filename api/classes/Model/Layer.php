<?php declare(strict_types=1);

namespace Ullallaa\Model;

use Db;
use Ullallaa\Database\DbQuery;
use Ullallaa\ObjectModel;

class Layer extends ObjectModel {
	/** @var $id Product ID */
	public $id;
	
    /** @var string $name */
	public $name;

	/** @var int $rug_part_id */
    public $rug_part_id;

    /** @var string $filename */
    public $filename;

	/**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'layers',
        'primary' => 'id',
        'fields' => array(
			'name' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isString', 'size' => 255),
            'rug_part_id' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'size' => 11),
            'filename' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isString', 'size' => 255),
        )
    );

     /**
     * constructor.
     *
     * @param null $id
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
	}
}