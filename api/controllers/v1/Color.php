<?php
/**
 * @copyright  2020 Ullallaa
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

namespace Ullallaa\v1;

use Db;
use Ullallaa\Route;
use Ullallaa\Database\DbQuery;
use Ullallaa\Model\Color as ColorObject;
use Ullallaa\Model\PartType as PartTypeObject;
use Ullallaa\Util\ArrayUtils;
use Ullallaa\Validate;

class Color extends Route {

	public function getColors() {
		$api = $this->api;

		// Build query
		$sql = new DbQuery();
		// Build SELECT
		$sql->select('colors.*, ptypes.name AS partname');
		// Build FROM
		$sql->from('colors', 'colors');
	
		$sql->innerJoin('part_types', 'ptypes', 'ptypes.id = colors.part_type_id');
	 	$order = 'part_type_id';
		$sql->orderBy($order);
		$colors = Db::getInstance()->executeS($sql);

		return $api->response([
			'success' => true,
			'colors' => $colors
		]);
	}

	public function getColor( $colorId ) {
		$api = $this->api;

		$color = new ColorObject( (int) $colorId );
		if(!Validate::isLoadedObject($color)) {
			$api->response->setStatus(404);
			return $api->response([
				'success' => false,
				'message' => 'Color was not found'
			]);
		}
		
		$parttype = new PartTypeObject( $color->part_type_id );

		return $api->response([
			'success' => true,
			'message' => 'Color was Created',
			'color' => [
				'id' => $color->id,
				'name' => $color->name,
				'colorcode' => $color->colorcode,
				'part_type' => [
					'id' => $parttype->id,
					'name' => $parttype->name
				],
			]
		]);
	}

	public function getColorByPartId ( $partId ) {
		$api = $this->api;

		$colorObject = new ColorObject();
		$colors = $colorObject->getColorByPartId($partId);

		return $api->response([
			'success' => true,
			'colors' => $colors
		]);
	}

	public function addColor() {
		$api = $this->api;
		$payload = $api->request()->post(); 

		$name = ArrayUtils::get($payload, 'name');
		$colorcode = ArrayUtils::get($payload, 'colorcode');
		$part_type_id = ArrayUtils::get($payload, 'part_type_id');

		if (!Validate::isGenericName($name)) {
			return $api->response([
				'success' => false,
				'message' => 'Enter a valid color name'
			]);
		}

		if (!Validate::isColorCode($colorcode)) {
			return $api->response([
				'success' => false,
				'message' => 'Enter a valid color code'
			]);
		}

		if(!Validate::isInt($part_type_id)) {
			return $api->response([
				'success' => false,
				'message' => 'Enter a valid part type ID of the product'
			]);
		}

		$color = new ColorObject();
		$color->name = $name;
		$color->colorcode = $colorcode;
		$color->part_type_id = $part_type_id;

		$ok = $color->save();
		// or $color->add();

		if (!$ok) {
			return $api->response([
				'success' => false,
				'message' => 'Unable to create product'
			]);
		}

		return $api->response([
			'success' => true,
			'message' => 'Color was Created',
			'color' => [
				'id' => $color->id,
				'name' => $color->name,
				'colorcode' => $color->colorcode,
				'part_type_id' => (float) $color->part_type_id,
			]
		]);
	}

	public function updateColor($colorId ) {
		$api = $this->api;
		$payload = $api->request()->post(); 

		$color = new ColorObject( (int) $colorId );
		if(!Validate::isLoadedObject($color)) {
			$api->response->setStatus(404);
			return $api->response([
				'success' => false,
				'message' => 'Color was not found'
			]);
		}

		if (ArrayUtils::has($payload, 'name')) {
			$name = ArrayUtils::get($payload, 'name');
			if ( !Validate::isGenericName($name) ) {
				return $api->response([
					'success' => false,
					'message' => 'Enter a valid color name'
				]);
			}

			$color->name = $name;
		}

		if (ArrayUtils::has($payload, 'colorcode')) {
			$colorcode = ArrayUtils::get($payload, 'colorcode');
			if (!Validate::isColorCode($colorcode)) {
				return $api->response([
					'success' => false,
					'message' => 'Enter a valid color code'
				]);
			}

			$color->colorcode = $colorcode;
		}

		if (ArrayUtils::has($payload, 'part_type_id')) {
			$part_type_id = ArrayUtils::get($payload, 'part_type_id');
			if(!Validate::isInt($part_type_id)) {
				return $api->response([
					'success' => false,
					'message' => 'Enter a valid part type ID of the color'
				]);
			}

			$parttype = new PartTypeObject( (int) $part_type_id );
			if (!Validate::isLoadedObject($parttype)) {
				return $api->response([
					'success' => false,
					'message' => 'The part type ID (' . $part_type_id . ') does not exist'
				]);
			}

			$color->part_type_id = $parttype->id;
		}

		return $api->response([
			'success' => false,
			'message' => 'Unable to update color'
		]);

		$ok = $color->save();
		// or product->update()
		
		if (!$ok) {
			return $api->response([
				'success' => false,
				'message' => 'Unable to update color'
			]);
		}

		return $api->response([
			'success' => true,
			'message' => 'Color updated successfully'
		]);
	}

	public function deleteColor( $colorId ) {
		$api = $this->api;

		$color = new ColorObject( (int) $colorId );
		if(!Validate::isLoadedObject($color)) {
			$api->response->setStatus(404);
			return $api->response([
				'success' => false,
				'message' => 'Color was not found'
			]);
		}

		$ok = $color->delete();

		if (!$ok) {
			return $api->response([
				'success' => false,
				'message' => 'Unable to delete color'
			]);
		}

		return $api->response([
			'success' => true,
			'message' => 'Color deleted successfully'
		]);
	}
	
	// public function searchProducts() {
	// 	$api = $this->api;
	// 	$params = $api->request()->get(); 

	// 	$name = ArrayUtils::get($params, 'name');
	// 	$description = ArrayUtils::get($params, 'description');

	// 	if(!$name && !$description) {
	// 		return $api->response([
	// 			'success' => false,
	// 			'message' => 'Enter name or description of the product'
	// 		]);
	// 	}

	// 	// Build query
	// 	$sql = new DbQuery();
	// 	// Build SELECT
	// 	$sql->select('product.*');
	// 	// Build FROM
	// 	$sql->from('product', 'product');

	// 	// prevent sql from searching a NULL value if wither name or description is not provided eg. WHERE name = null
	// 	$where_clause = array();
	// 	if($name) {
	// 		$where_clause[] = 'product.name LIKE \'%' . pSQL($name) . '%\'';
	// 	}

	// 	if ($description) {
	// 		$where_clause[] = 'product.description LIKE \'%' . pSQL($description) . '%\'';
	// 	}

	// 	// join the search terms
	// 	$where_clause = implode(' OR ', $where_clause);

	// 	// Build WHERE
	// 	$sql->where($where_clause);

	// 	$products = Db::getInstance()->executeS($sql);

	// 	return $api->response([
	// 		'success' => true,
	// 		'products' => $products
	// 	]);
	// }
}


