<?php
/**
 * @copyright  2020 Ullallaa
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

namespace Ullallaa\v1;

use Db;
use Ullallaa\Route;
use Ullallaa\Database\DbQuery;
use Ullallaa\Model\Color as ColorObject;
use Ullallaa\Model\Layer as LayerObject;
use Ullallaa\Model\Rug as RugObject;
use Ullallaa\Model\RugPart as RugPartObject;
use Ullallaa\Model\PartType as PartTypeObject;
use Ullallaa\Util\ArrayUtils;
use Ullallaa\Validate;
use Ullallaa\Engine\RugFactory;

class Rug extends Route {

	public function getRug($rugid) {

		$api = $this->api;

		$rug = new RugObject($rugid);
		if(!Validate::isLoadedObject($rug)) {
			$api->response->setStatus(404);
			return $api->response([
				'success' => false,
				'message' => 'Rug was not found'
			]);
		}
		$rugjson = $rug->rug_parts;
		$rugfactory = new RugFactory();
		$ok = $rugfactory->loadComponents($rugjson);
		$outrug = $rugfactory->getRug();

		return $api->response([
			'success' => true,
			'message' => 'Rug',
			'rug' => [
				'id' => $rug->id,
				'name' => $rug->name,
				'imagedata' => $outrug['data'],
				'json' => $outrug['json']
			]
		]);
		
	}

	public function addRug() {

		$api = $this->api;
		$payload = $api->request()->post(); 
		$name = ArrayUtils::get($payload, 'name');

		$rugfactory = new RugFactory();
		$rug = $rugfactory->initRug($name);

		if (!$rug) {
			return $api->response([
				'success' => false,
				'message' => 'Unable to create rug'
			]);
		}

		return $api->response([
			'success' => true,
			'message' => 'Rug was Created',
			'rug' => [
				'id' => $rug->id,
				'name' => $rug->name,
			]
		]);
	}

	public function patchRug($rugid) {

		$api = $this->api;
		$payload = $api->request()->post(); 

		$part_type_id = ArrayUtils::get($payload, 'part_type_id');
		$colorid = ArrayUtils::get($payload, 'color_id');
		$layerid = ArrayUtils::get($payload, 'layer_id');
		$parent_layerid = ArrayUtils::get($payload, 'parent_layer_id');
		$isparent = false;
		$ischild = false;

		$part_type = new PartTypeObject($part_type_id);
		if(!Validate::isLoadedObject($part_type)) {
			$api->response->setStatus(404);
			return $api->response([
				'success' => false,
				'message' => 'Part type was not found'
			]);
		} else {
			switch ($part_type->withparent) {
				case -1:
					$isparent = true;
					$ischild = false;
					break;
				case 0:
					$isparent = false;
					$ischild = false;
					break;
				case 1:
					$isparent = false;
					$ischild = true;
					break;
			} 
		}

		if (isset($colorid)) {
			$color = new ColorObject($colorid);
			if(!Validate::isLoadedObject($color)) {
				$api->response->setStatus(404);
				return $api->response([
					'success' => false,
					'message' => 'Color was not found'
				]);
			}
		}

		$outlayers = Array();
		if (isset($layerid)) {
			$layer = new LayerObject($layerid);
			if(!Validate::isLoadedObject($layer)) {
				$api->response->setStatus(404);
				return $api->response([
					'success' => false,
					'message' => 'Layer was not found'
				]);
			} else {
				$outlayers[] = $layerid;
			}
		} else {
			// we set the default layer for this step
			$sql = new DbQuery();
			// Build SELECT
			$sql->select('rug_parts.*');
			// Build FROM
			$sql->from('rug_parts', 'rug_parts');
			
			$where = 'rug_parts.type_id = ' . $part_type_id;
			if ($ischild) $where .= ' AND rug_parts.parent = ' . $parent_layerid;

			$sql->where($where);
			$rugpart = Db::getInstance()->executeS($sql);

			/////////////////////////////////////////
			if (count($rugpart) > 0) {
				$sql = new DbQuery();
				// Build SELECT
				$sql->select('layers.*');
				// Build FROM
				$sql->from('layers', 'layers');
				$where = 'layers.rug_part_id = ' . $rugpart[0]['id'];
				$sql->where($where);
				$layers = Db::getInstance()->executeS($sql);
				foreach ($layers as &$layer) {
					$outlayers[] = $layer['id'];
				}
			}

		}

		$rug = new RugObject($rugid);
		if(!Validate::isLoadedObject($rug)) {
			$api->response->setStatus(404);
			return $api->response([
				'success' => false,
				'message' => 'Rug was not found'
			]);
		}

		$rugjson = $rug->rug_parts;
		$rugfactory = new RugFactory();
		$ok = $rugfactory->loadComponents($rugjson);

		$rugfactory->addComponent($part_type_id, $colorid, $outlayers, $isparent, $ischild);

		$outrug = $rugfactory->getRug();
		$rug->rug_parts = $outrug['json'];
		$rug->save();

		return $api->response([
			'success' => true,
			'message' => 'Rug was patched',
			'rug' => [
				'id' => $rug->id,
				'name' => $rug->name,
				'imagedata' => $outrug['data'],
				'json' => $outrug['json']
			]
		]);
	}

	public function getFrontSteps() {

		$api = $this->api;

		// Build query
		$sql = new DbQuery();
		// Build SELECT
		$sql->select('part_types.*');
		// Build FROM
		$sql->from('part_types', 'part_types');
	
	 	$order = 'part_types.id';
		$sql->orderBy($order);
		
		$frontsteps = Db::getInstance()->executeS($sql);

		return $api->response([
			'success' => true,
			'frontsteps' => $frontsteps
		]);
	}

}


