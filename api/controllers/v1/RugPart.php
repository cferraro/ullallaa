<?php
/**
 * @copyright  2020 Ullallaa
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

namespace Ullallaa\v1;

use Db;
use Ullallaa\Route;
use Ullallaa\Database\DbQuery;
use Ullallaa\Model\RugPart as RugPartObject;
use Ullallaa\Util\ArrayUtils;
use Ullallaa\Validate;

class RugPart extends Route {

	public function addRugPart() {
		$api = $this->api;
		$payload = $api->request()->post();
		$name = ArrayUtils::get($payload, 'name');
		$type_id = ArrayUtils::get($payload, 'type_id');

		if (!Validate::isGenericName($name)) {
			return $api->response([
				'success' => false,
				'message' => 'Enter a valid rug part name'
			]);
		}
		if (!Validate::isInt($type_id)) {
			return $api->response([
				'success' => false,
				'message' => 'Enter a valid type'
			]);
		}

		$rugpart = new RugPartObject();
		$rugpart->name = $name;
		$rugpart->type_id = $type_id;

		$ok = $rugpart->save();

		if (!$ok) {
			return $api->response([
				'success' => false,
				'message' => 'Unable to create Rug Part'
			]);
		}

		return $api->response([
			'success' => true,
			'message' => 'Rug Part was Created',
			'layer' => [
				'id' => $rugpart->id,
				'name' => $rugpart->name,
				'type_id' => (float) $rugpart->type_id,
			]
		]);
	}

	public function getRugPartByTypeId($typeId) {
		
		$api = $this->api;
		$sql = new DbQuery();
		// Build SELECT
		$sql->select('rug_parts.*');
		// Build FROM
		$sql->from('rug_parts', 'rug_parts');

		$where_clause = array();
		if($typeId) {
			$where_clause = 'type_id = ' . pSQL($typeId);
		}
		$sql->where($where_clause);
		$rug_parts = Db::getInstance()->executeS($sql);
        
		return $api->response([
			'success' => true,
			'rugparts' => $rug_parts 
		]);

	}

	public function deleteRugPart( $rugpartId ) {
		$api = $this->api;

		$layer = new LayerObject( (int) $layerId );
		if(!Validate::isLoadedObject($layer)) {
			$api->response->setStatus(404);
			return $api->response([
				'success' => false,
				'message' => 'Layer was not found'
			]);
		}

		$ok = $layer->delete();

		if (!$ok) {
			return $api->response([
				'success' => false,
				'message' => 'Unable to delete layer'
			]);
		}

		return $api->response([
			'success' => true,
			'message' => 'Layer deleted successfully'
		]);
	}
	

}


