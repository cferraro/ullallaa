<?php
/**
 * @copyright  2020 Ullallaa
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

namespace Ullallaa\v1;

use Db;
use Ullallaa\Route;
use Ullallaa\Database\DbQuery;
use Ullallaa\Model\Color as ColorObject;
use Ullallaa\Model\PartType as PartTypeObject;
use Ullallaa\Util\ArrayUtils;
use Ullallaa\Validate;

class FrontStep extends Route {

	public function getFrontSteps() {

		$api = $this->api;

		// Build query
		$sql = new DbQuery();
		// Build SELECT
		$sql->select('part_types.*');
		// Build FROM
		$sql->from('part_types', 'part_types');
	
	 	$order = 'part_types.id';
		$sql->orderBy($order);
		$frontsteps = Db::getInstance()->executeS($sql);

		return $api->response([
			'success' => true,
			'frontsteps' => $frontsteps
		]);
	}

	public function getFrontStepDataByStepId( $stepId, $parentRugPart = null ) {
		$api = $this->api;

		$parttypeObject = new PartTypeObject();
		$parttype = $parttypeObject->getPartTypeByStep($stepId);

		$parttypeId = null;
		$colors = null;
		if ($parttype != null) {
			$parttypeId = $parttype[0]['id'];
			$colorObject = new ColorObject();
			$colors = $colorObject->getByPartType($parttypeId);
		
			/* RUG PARTS */
			$sql = new DbQuery();
			// Build SELECT
			$sql->select('rug_parts.*');
			// Build FROM
			$sql->from('rug_parts', 'rug_parts');
			if ($parttype[0]['withparent'] == '1') {
				if($parentRugPart != null) {
					$sql->where('rug_parts.parent = ' . pSQL($parentRugPart) . ' AND rug_parts.type_id = ' . pSQL($parttypeId));
				}
			} else {
				$sql->where('rug_parts.type_id = ' . pSQL($parttypeId));
			}
			
			$rugpart = Db::getInstance()->executeS($sql);

			$layers = null;
			if ($rugpart != null) {

				/* LAYERS */
				$sql = new DbQuery();
				// Build SELECT
				$sql->select('layers.*');
				// Build FROM
				$sql->from('layers', 'layers');

				$where = '';
				$index = 0;
				foreach ($rugpart as &$part) {
					$where .= ($index > 0 && count($rugpart) > $index) ? ' OR ' : '';
					if($part['id']) {
						$where .= 'layers.rug_part_id = ' . pSQL($part['id']);
					}
					$index++;
				}
				$sql->where($where);
				$layers = Db::getInstance()->executeS($sql);
			}

			return $api->response([
				'success' => true,
				'message' => 'Step fetched',
				'parttype' => $parttype,
				'colors' => $colors,
				'rugpart' => $rugpart,
				'layers' => $layers
			]);
		
		} else {
			return $api->response([
				'success' => false,
				'message' => 'Step not found'
			]);
		}
	}

}


