<?php
/**
 * @copyright  2020 Ullallaa
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

namespace Ullallaa\v1;

use Db;
use Ullallaa\Route;
use Ullallaa\Database\DbQuery;
use Ullallaa\Model\PartType as PartTypeObject;
use Ullallaa\Util\ArrayUtils;
use Ullallaa\Validate;

class PartType extends Route {

	public function getPartTypes() {

		$api = $this->api;

		// Build query
		$sql = new DbQuery();
		// Build SELECT
		$sql->select('part_types.*');
		// Build FROM
		$sql->from('part_types', 'part_types');
		
		$order = 'part_types.orderindex';
		$sql->orderBy($order);

		$parttypes = Db::getInstance()->executeS($sql);

		return $api->response([
			'success' => true,
			'parttypes' => $parttypes
		]);
	}


}


