<?php
/**
 * @copyright  2020 Ullallaa
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

namespace Ullallaa\v1;

define ('SITE_ROOT', __DIR__ . '/../../..');
define ('UPLOAD_LAYER_DIR', '/uploads/layers');

use Db;
use Ullallaa\Route;
use Ullallaa\Database\DbQuery;
use Ullallaa\Model\Layer as LayerObject;
use Ullallaa\Model\RugPart as RugPartObject;
use Ullallaa\Model\PartType as PartTypeObject;
use Ullallaa\Util\ArrayUtils;
use Ullallaa\Validate;

class Layer extends Route {

	public function getLayers() {
		$api = $this->api;

		// Build query
		$sql = new DbQuery();
		// Build SELECT
		$sql->select('layers.*');
		// Build FROM
		$sql->from('layers', 'layers');
	
		$layers = Db::getInstance()->executeS($sql);

		return $api->response([
			'success' => true,
			'layers' => $layers
		]);
	}

	
	public function getLayersByPartTypeId($partTypeId) {
		$api = $this->api;
		// Build query
		$sql = new DbQuery();
		// Build SELECT
		$sql->select('layers.*, rug_parts.type_id');
		// Build FROM
		$sql->from('layers', 'layers');

		$sql->leftJoin('rug_parts', 'rug_parts', 'rug_parts.id = layers.rug_part_id');
		$sql->where('rug_parts.type_id = ' . $partTypeId);
		$layers = Db::getInstance()->executeS($sql);
		
		return $api->response([
			'success' => true,
			'layers' => $layers
		]);
	}


	public function getLayersByRugPartId($rugPartId, $headers = true) {

		$api = $this->api;
		// Build query
		$sql = new DbQuery();
		// Build SELECT
		$sql->select('layers.*');
		// Build FROM
		$sql->from('layers', 'layers');

		$sql->where('layers.rug_part_id = ' . $rugPartId);
		$layers = Db::getInstance()->executeS($sql);
		if ($headers) { 
			return $api->response([
				'success' => true,
				'layers' => $layers
			]);
		} else {
			return [
				'success' => true,
				'layers' => $layers
			];
		}
	}

	public function addLayer() {

		$api = $this->api;
		$payload = $api->request()->post(); 

		$name = ArrayUtils::get($payload, 'name');
		$type_id = ArrayUtils::get($payload, 'type_id');
		$rug_part_id = ArrayUtils::get($payload, 'rug_part_id');
		$parent_id = ArrayUtils::get($payload, 'parent_id');

		$filename = $this->getFileFromRequest('filename');
		
		if (!Validate::isGenericName($name)) {
			$api->response->setStatus(400);
			return $api->response([
				'success' => false,
				'message' => 'Enter a valid layer name'
			]);
		}
		if (file_exists($this->getLayerPath() . $filename)) {
			$extension = $this->get_file_extension($filename);
			if ($extension != 'png') {
				//unlink($this->getLayerPath() . $filename);
				$api->response->setStatus(400);
				return $api->response([
					'success' => false,
					'message' => 'File extension not supported'
				]);				
			}
		} else {
			$api->response->setStatus(400);
			return $api->response([
				'success' => false,
				'message' => 'Enter a file'
			]);
		}
		$filename = UPLOAD_LAYER_DIR . '/' . $filename;
		
		if (!Validate::isInt($parent_id)) {
			//return $api->response([
			//	'success' => false,
			//	'message' => 'The Rug Part Parent (' . $parent_id . ') does not exist'
			//]);
		}

		$rugpart = null;
		if (!Validate::isInt($rug_part_id)) {
			$rugpart = new RugPartObject();
			$result = $rugpart->addRugPart('Rugpart ' . $name, $type_id, $parent_id);
			if ($result != null)
			{
				$rug_part_id = $result;
			}
		} else {
			$rugpart = new RugPartObject( (int) $rug_part_id );
			if (!Validate::isLoadedObject($rugpart)) {
				$api->response->setStatus(404);
				return $api->response([
					'success' => false,
					'message' => 'The Rug Part Group (' . $rug_part_id . ') does not exist'
				]);
			}
		}

		$layer = new LayerObject();
		$layer->name = $name;
		$layer->filename = $filename;
		$layer->rug_part_id = $rug_part_id;

		$ok = $layer->save();
		// or $color->add();

		if (!$ok) {
			$api->response->setStatus(500);
			return $api->response([
				'success' => false,
				'message' => 'Unable to create layer'
			]);
		}

		return $api->response([
			'success' => true,
			'message' => 'Layer was Created',
			'layer' => [
				'id' => $layer->id,
				'name' => $layer->name,
				'filename' => $layer->filename,
				'rug_part_id' => (float) $layer->rug_part_id,
				'rug_part' => [
					'id' => $rugpart->id,
					'name' => $rugpart->name,
					'type_id' => $rugpart->type_id
				]
			]
		]);
	}

	public function deleteLayer( $layerId ) {
		$api = $this->api;

		$layer = new LayerObject( (int) $layerId );
		if(!Validate::isLoadedObject($layer)) {
			$api->response->setStatus(404);
			return $api->response([
				'success' => false,
				'message' => 'Layer was not found'
			]);
		}
		$rug_part_id = $layer->rug_part_id;
		$otherLayersRes = $this->getLayersByRugPartId($rug_part_id, false);
		if ($otherLayersRes['success']) {
			if (count($otherLayersRes['layers']) == 1) {
				$rugpart = new RugPartObject((int) $rug_part_id);
				if(Validate::isLoadedObject($rugpart)) {
					$rugpart->delete();
				}
			}
		}

		$ok = $layer->delete();

		if (!$ok) {
			$api->response->setStatus(500);
			return $api->response([
				'success' => false,
				'message' => 'Unable to delete layer'
			]);
		}

		return $api->response([
			'success' => true,
			'message' => 'Layer deleted successfully'
		]);
	}


	public function getFileFromRequest($fieldName) {
		if ($_FILES["filename"]["error"] == UPLOAD_ERR_OK) {
			$tmp_name = $_FILES["filename"]["tmp_name"];
			$name = basename($_FILES["filename"]["name"]);
			$renamedFile = $this->getRandomName(18) . '.' . $this->get_file_extension($name);
			move_uploaded_file($tmp_name, $this->getLayerPath() . "$renamedFile");
			return $renamedFile;
		}
		return '';
	}

	private function get_file_extension($file_name) {
    	return substr(strrchr($file_name,'.'),1);
	}

	private function getRandomName($n) { 
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
		$randomString = ''; 
	
		for ($i = 0; $i < $n; $i++) { 
			$index = rand(0, strlen($characters) - 1); 
			$randomString .= $characters[$index]; 
		} 
    	return $randomString; 
	} 
	
	private function getLayerPath() {
		return SITE_ROOT . UPLOAD_LAYER_DIR . "/";
	}
}


