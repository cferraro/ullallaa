<?php declare(strict_types=1);

namespace Ullallaa\Rugs\Engine;

use Ullallaa\Rugs\Parts\Rug;
use Ullallaa\Rugs\Builders\RugBuilder;
/**
 * 
 */
class PartFactory
{
    private $builder;

    public function initRug(Builder $builder): Rug
    {
        $this->builder = $builder;
        return $builder->getRug();
    }

    public function addComponent() {

    }

    public function getPartFactory($step_number) {
        
    }
}