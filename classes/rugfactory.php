<?php declare(strict_types=1);

namespace Ullallaa\Rugs\Engine;

use Ullallaa\Rugs\Parts\Rug;
use Ullallaa\Rugs\Builders\RugBuilder;
/**
 * 
 */
class RugFactory
{
    private $builder;

    public function initRug(Builder $builder): Rug
    {
        $this->builder = $builder;
        return $builder->getRug();
    }

    public function addComponent() {

    }

    public function getComponentFactory($step_number) {
        
    }
}