<?php declare(strict_types=1);

use Ullallaa\Rugs\Parts;

abstract class Rug
{
    /**
     * @var object[]
     */
    private array() $data = [];

    public function setPart(string $key, object $value)
    {
        $this->data[$key] = $value;
    }
}