<?php declare(strict_types=1);

namespace Ullallaa\Rugs\Builders;

use Ullallaa\Rugs\Parts\Rug;

interface Builder
{
    public function initRug();
    
    public function createRug();

    public function addPattern(); // layers separated

    public function addWarp();

    public function addWarpColors();

    public function addYarn();

    public function addYarnColors();

    public function addBorder();

    public function addBorderColors();

    public function addRope();

    public function addRopeColors();

    public function getRug(): Rug;
}