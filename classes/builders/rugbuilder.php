<?php declare(strict_types=1);

namespace Ullallaa\Rugs\Builders;

use Ullallaa\Rugs\Parts\Border;
use Ullallaa\Rugs\Parts\Pattern;
use Ullallaa\Rugs\Parts\Rope;
use Ullallaa\Rugs\Parts\Rug;
use Ullallaa\Rugs\Parts\Warp;
use Ullallaa\Rugs\Parts\Yarn;

class RugBuilder implements Builder
{
    private Rug $rug;

    public function initRug()
    {

        
    }

    public function addPattern()
    {
        $this->rug->setPart('pattern1', new Pattern());
        $this->rug->setPart('pattern2', new Pattern());
        $this->rug->setPart('pattern3', new Pattern());
    }

    public function addWarp()
    {
        $this->rug->setPart('warp', new Warp());
    }

    public function addWarpColor()
    {
        $this->rug->setPart('warp-color', new Color());
    }

    public function addYarn()
    {
        $this->rug->setPart('yarn1', new Yarn());
        $this->rug->setPart('yarn2', new Yarn());
        $this->rug->setPart('yarn3', new Yarn());
        $this->rug->setPart('yarn4', new Yarn());
    }

    public function addYarnColor()
    {
        $this->rug->setPart('yarn-color', new Color());
    }

    public function addBorder()
    {
        $this->rug->setPart('border1', new Border());
        $this->rug->setPart('border2', new Border());
        $this->rug->setPart('border3', new Border());
        $this->rug->setPart('border4', new Border());
    }

    public function addBorderColor()
    {
        $this->rug->setPart('border-color', new Color());
    }

    public function addRope()
    {
        $this->rug->setPart('rope1', new Rope());
        $this->rug->setPart('rope2', new Rope());
        $this->rug->setPart('rope3', new Rope());
        $this->rug->setPart('rope4', new Rope());
    }

    public function addRopeColor()
    {
        $this->rug->setPart('rope-color', new Color());
    }

    public function createRug()
    {
        $this->rug = new Rug();
    }

    public function getRug(): Rug
    {
        return $this->rug;
    }
}