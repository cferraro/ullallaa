var designImage = document.getElementById('design-image');
var basePath = '';
var currentRugId;
var currentStep;
var stepsIds = [];
var parentLayer;

// UNIT TEST

var showRugBtn = document.getElementById('show-rug-btn');
var rugIdfield = document.getElementById('rug-id-field');

showRugBtn.addEventListener('click', function() {
    var rugId = rugIdfield.value;
    if (Number.isInteger(parseInt(rugId))) {
        showRug(rugId);
    }
});
function showRug(rugId) {
    var r = new XMLHttpRequest();
    r.open("GET", basePath + "api/v1/rug/" + rugId, true);
    r.onreadystatechange = function () {
    
    if (r.readyState != 4 || r.status != 200) return;
    const rugObject = JSON.parse(r.responseText);
        var rug = rugObject.rug;
        var rugname = rug.name;
        var imageData = rug.imagedata;
        designImage.setAttribute('src', 'data:image/png;base64,' + imageData);
    };
    r.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    r.send();
}

// END UNIT TEST

var LayerManager = ( function() {
    
    var layerContainer = document.querySelector('.layers-container');
    var rugpartItem = layerContainer.querySelector('.rug-part-item');
    var newItem = rugpartItem.cloneNode(true);
    rugpartItem.remove();

    function initLayers(layers, part, parent) {

        layerContainer.style.display = (layers && layers.length > 0) ? 'block' : 'none';
        layerContainer.innerHTML = '';

        if (parent > -1) {
            layerContainer.style.display = 'none';
            if (layers) layers.forEach(populateDesignArea);
        } else {
            if (layers) layers.forEach(addLayerItem);
        }

    }

    function populateDesignArea(item, index) {
        //console.log(item);
        patchRug(item.id);
    }

    function addLayerItem(item, index) {
        layerContainer.style.display = 'block';
        let nextitem = newItem.cloneNode(true);
        newItem.querySelector('.layer-image').src = '/ullalla' + item.filename;
        newItem.setAttribute('data-id', item.rug_part_id);
        newItem.addEventListener('click', clickLayerItem);
        layerContainer.appendChild(newItem);
        newItem = nextitem;
    }

    function clickLayerItem(e) {
        var item = e.target;
        var selectedId = item.parentNode.parentNode.getAttribute('data-id');
        parentLayer = selectedId;
        Stepper.nextStep();
        //patchRug(selectedId);
    }

    function patchRug(selectedId) {

        formData = { 'part_type_id' : stepsIds[currentStep], 'layer_id' : selectedId }

        var r = new XMLHttpRequest();
        r.open("PATCH", basePath + "api/v1/rug/" + currentRugId, true);
        r.onreadystatechange = function () {
          
        if (r.readyState != 4 || r.status != 200) return;
        const rugObject = JSON.parse(r.responseText);
          var rug = rugObject.rug;
          var rugname = rug.name;
          var imageData = rug.imagedata;
          setImageData(imageData);

        };
        r.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        r.send(JSON.stringify(formData));
    }

    function setImageData(data) {
        designImage.setAttribute('src', 'data:image/png;base64,' + data);
    }

    return {
        initLayers: initLayers
    }
})();


var Palette = ( function() {

    var paletteContainer = document.querySelector('.palette-container');
    var paletteItem = paletteContainer.querySelector('.palette-item');
    var newItem = paletteItem.cloneNode(true);
    paletteItem.remove();

    function initPalette(colors) {
        paletteContainer.style.display = (colors && colors.length > 0) ? 'block' : 'none';
        paletteContainer.innerHTML = '';
        colors.forEach(addPaletteItem);
    }

    function addPaletteItem(item, index) {
        let nextitem = newItem.cloneNode(true);
        newItem.querySelector('.color-item').style.backgroundColor = item.colorcode;
        newItem.setAttribute('data-id', item.id);
        newItem.addEventListener('click', clickPaletteItem);
        paletteContainer.appendChild(newItem);
        newItem = nextitem;
    }

    function clickPaletteItem(e) {
        var item = e.target;
        var selectedId = item.parentNode.getAttribute('data-id');
        patchRug(selectedId);
    }

    function patchRug(selectedId) {

        formData = { 'part_type_id' : stepsIds[currentStep], 
                     'color_id' : selectedId,
                     'parent_layer_id' : parentLayer }

        var r = new XMLHttpRequest();
        r.open("PATCH", basePath + "api/v1/rug/" + currentRugId, true);
        r.onreadystatechange = function () {
          
        if (r.readyState != 4 || r.status != 200) return;
        const rugObject = JSON.parse(r.responseText);
          var rug = rugObject.rug;
          var rugname = rug.name;
          var imageData = rug.imagedata;
          setImageData(imageData);

        };
        r.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        r.send(JSON.stringify(formData));
    }

    function setImageData(data) {
        designImage.setAttribute('src', 'data:image/png;base64,' + data);
    }

    return {
        initPalette: initPalette
    }
})();

var Stepper = ( function() {

    var data = {};
    var stepTitle = document.querySelector('.step-title');
    var nextBtn = document.getElementById('next-step-btn');
    var finishBtn = document.getElementById('finish-step-btn');
    var layerContainer = document.querySelector('.layers-container');
    var paletteContainer = document.querySelector('.palette-container');
    nextBtn.addEventListener('click', gotoNextStep);
    finishBtn.addEventListener('click', finalizeWizard);

    function initStep(step) {

        var parentLayerQuery = (parentLayer) ? '/' + parentLayer : '';

        nextBtn.style.display = 'block';
        currentStep = step;
        var s = new XMLHttpRequest();
        s.open("GET", basePath + "api/v1/frontsteps/" + stepsIds[step] + parentLayerQuery, true);
        s.onreadystatechange = function () {
          
        if (s.readyState != 4 || s.status != 200) return;
           const stepObject = JSON.parse(s.responseText);
           data = stepObject;
           Palette.initPalette(data.colors);
           stepTitle.innerHTML = data.parttype[0].name;
           
           LayerManager.initLayers(data.layers, data.rugpart, parseInt(data.parttype[0].withparent));
        };
        s.send();
    }

    function loadSteps() {

        var s = new XMLHttpRequest();
        s.open("GET", basePath + "api/v1/parttypes", true);
        s.onreadystatechange = function () {
          
        if (s.readyState != 4 || s.status != 200) return;
            const parttypeObject = JSON.parse(s.responseText);
            data = parttypeObject;
            data.parttypes.forEach(function(item, index) { stepsIds.push(item.id) });
        };
        s.send();
    }

    function gotoNextStep() {
        currentStep++;
        initStep(currentStep);
        if (currentStep >= (stepsIds.length - 1)) {
            nextBtn.style.display = 'none';
            finishBtn.style.display = 'block';
        } 
    }

    function finalizeWizard() {
        layerContainer.style.display = 'none';
        paletteContainer.style.display = 'none';
    }

    return {
        loadStep: initStep,
        stepData: data,
        loadAllSteps: loadSteps,
        nextStep: gotoNextStep
    }
})();




var RugPlugin = ( function() {

    function init(step) {
        Stepper.loadStep(step);
    }

    return {
        initialize: init
    }
})();


var PluginContainer = ( function() {

    var stepContainer = document.querySelector('.step-container');
    var layerContainer = document.querySelector('.layers-container');
    var paletteContainer = document.querySelector('.palette-container');
    var startBtn = document.querySelector('.create-rug-btn');
    var nextBtn = document.getElementById('next-step-btn');
    var finishBtn = document.getElementById('finish-step-btn');
    var rugnameInput = document.getElementById('rug-name-field');
    

    function initEnvironment() {
        stepContainer.style.display = 'block';
        layerContainer.style.display = 'none';
        paletteContainer.style.display = 'none';
        nextBtn.style.display = 'none';
        finishBtn.style.display = 'none';
        startBtn.addEventListener('click', initializePlugin);

        Stepper.loadAllSteps();
    }

    function initializePlugin() {
        stepContainer.style.display = 'none';
        createRug();
    }
    
    function createRug() {

        var rugname = rugnameInput.value;
        var formData = new FormData();
        formData.append('name', rugname);

        var r = new XMLHttpRequest();
        r.open("POST", basePath + "api/v1/rug", true);
        r.onreadystatechange = function () {
          
        if (r.readyState != 4 || r.status != 200) return;
        const rugObject = JSON.parse(r.responseText);
          var rug = rugObject.rug;
          currentRugId = rug.id;

          RugPlugin.initialize(0);
        };
        r.send(formData);

    }
    return {
        initenv: initEnvironment
    }

})();
PluginContainer.initenv();

